import os, subprocess, sys, random, datetime
sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..', '..', '..', 'tools'))
sys.path.append(os.path.join(os.environ.get("SUMO_HOME", os.path.join(os.path.dirname(__file__), '..', '..', '..')), 'tools'))
from sumolib import checkBinary
import sumolib
import traci

PORT = 9999

sumoBinary = checkBinary('sumo-gui')
#Mudar para o nome do arquivo sumocfg que vc estiver usando
sumoConfig = "osm.sumocfg"

if len(sys.argv) > 1:
    huepath = '"%s" -c "%s" --python-script "%s" --remote-port %s' % (sumoBinary, sumoConfig, __file__, PORT)
    print(huepath)
    retCode = subprocess.call(huepath, shell=True, stdout=sys.stdout)
    sys.exit(retCode)
else:
    huepath = '"%s" -c "%s" --remote-port %s' % (sumoBinary, sumoConfig, PORT)
    print(huepath)
    sumoProcess = subprocess.Popen(huepath, shell=True, stdout=sys.stdout)
    traci.init(PORT)

print("\nTraci Iniciado")

step = 0
#Pode o True por uma condicao abaixo por alguma condicao de parada
while True:
    traci.simulationStep()
    startTime = datetime.datetime.now()
    print "Iniciando step " + str(step)     
    
    #Seu codigo vai aqui
        
    #tempo em ms que levou para rodar o step
    timeSpent = (datetime.datetime.now() - startTime).microseconds / 1000
    print "Step " + str(step) + " finalizada em " + str(timeSpent) + "ms"
    step = step+1
    