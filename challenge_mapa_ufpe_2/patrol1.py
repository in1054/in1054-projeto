print("starting...")

import os, subprocess, sys, random
sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..', '..', '..', 'tools'))
sys.path.append(os.path.join(os.environ.get("SUMO_HOME", os.path.join(os.path.dirname(__file__), '..', '..', '..')), 'tools'))
from sumolib import checkBinary
import sumolib
import traci
import patrolVehicle as in1054
import paho.mqtt.client as mqtt
import time
import json

PORT = 9999

NSGREEN = "GrGr"
NSYELLOW = "yryr"
WEGREEN = "rGrG"
WEYELLOW = "ryry"

PROGRAM = [WEYELLOW,WEYELLOW,WEYELLOW,NSGREEN,NSGREEN,NSGREEN,NSGREEN,NSGREEN,NSGREEN,NSGREEN,NSGREEN,NSYELLOW,NSYELLOW,WEGREEN]
sumoBinary = checkBinary('sumo-gui')
#sumoConfig = "data/Cologne.sumocfg"
#sumoConfig = "E:\\Doc\\veins-veins-4.4\\examples\\veins\\erlangen.sumo.cfg"
sumoConfig = "osm.sumocfg"

if len(sys.argv) > 1:
    huepath = '"%s" -c "%s" --python-script "%s" --remote-port %s' % (sumoBinary, sumoConfig, __file__, PORT)
    #print(huepath)
    retCode = subprocess.call(huepath, shell=True, stdout=sys.stdout)
    #print("Subproc called")
    sys.exit(retCode)
else:
    huepath = '"%s" -c "%s" --remote-port %s' % (sumoBinary, sumoConfig, PORT)
    #print(huepath)
    sumoProcess = subprocess.Popen(huepath, shell=True, stdout=sys.stdout)
    #print("\nSumo Popen")
    traci.init(PORT)
    #print("\nTraCI init")

#print("\nTraci Iniciado")
traci.gui.setZoom("View #0", 400)

traci.simulationStep()
programPointer = len(PROGRAM)-1
i=0
s=0
t=0
k=0
CTT5cars=[0,0,0,0,0]
step = 0
VEH=[]
EDG=[]
TIM=[]
VehCTT=[]
ED=[]
ITT=[]
V=[]
CTT=[]
Jun=[]
L_Veh=[]
Trav_tim=[]
VehiD=["0","1"]
Curr_Trav=[]
Trav_tim_sys=[]
A=[]
i = 0

All = traci.lane.getIDList()

NA=len(All)
#print "NA iss", NA

for s in range(0,NA):
    LA=traci.lane.getLength(All[s])
    if LA>100:
        A.append(All[s])

##################################################

for i in range(0,len(A)):
    L=traci.lane.getLength(A[i]) # Get the Length of all Lane
    V=traci.lane.getMaxSpeed(A[i]) # Get Maximum Allowed speed
    ITT= L/V
    # Set ITT for Lanes
    ED= traci.lane.getEdgeID(A[i])
    traci.edge.setEffort(ED, ITT)
    CTT5cars=[ITT,ITT,ITT,ITT,ITT]
    CTTcheck=0

# Simulation starts here:
PontosInteresseVermelho = ["257975070#0", "249088920", "-248091833#0", "317078453#2"]
PontosInteresseAzul = ["257975070#5-AddedOnRampEdge", "248109372#0", "199429353", "199429358#1", "199428975#0"]
PontosInteresseVerde = ["199429358#6", "248096097#2", "245730591#1", "249100222", "249099950"]

in1054.PatrolRouteCollection.Instance().addPatrolRoute("CCB", PontosInteresseVermelho)
in1054.PatrolRouteCollection.Instance().addPatrolRoute("CCSA", PontosInteresseAzul)
in1054.PatrolRouteCollection.Instance().addPatrolRoute("CCEN", PontosInteresseVerde)

patrolCars = in1054.PatrolVehicleCollection.Instance()

patrolCars.addCar(in1054.PatrolVehicle("segurancaUFPE1", "CCB"), (255,0,0,0), True)
patrolCars.addCar(in1054.PatrolVehicle("segurancaUFPE2", "CCSA"), (0,255,255,255), True)
patrolCars.addCar(in1054.PatrolVehicle("segurancaUFPE3", "CCEN"), (0,255,0,0), True)

######################################################
# MQTT                                               #
######################################################
clientName = "in1054-"  + str(random.randint(1, 100)) #(Change this as same names will conflict)

#device setup
j = """
{
    "deviceInfo": {
        "status": "good",
        "color": "#4D90FE",
        "endPoints": {
            "voltas": {
                "units": "ciclos",
                "values": {
                    "value": 0
                },
                "card-type": "crouton-simple-text",
                "title": "Ciclos completados na patrulha atual"
            },
            "velocidade": {
                "values": {
                    "labels": [1],
                    "series": [[60]],
                    "update": ""
                },
                "max": 20,
                "low": 0,
                "high": 60,
                "card-type": "crouton-chart-line",
                "title": "Velocidade (Km/h)"
            },
            "rotas": {
                "units": "",
                "values": {
                    "value": ""
                },
                "card-type": "crouton-simple-text",
                "title": "Rotas cobertas"
            }
        },
        "description": "Veiculo de patrulha"
    }
}

"""

device = json.loads(j)
device["deviceInfo"]["name"] = clientName
deviceJson = json.dumps(device)

#brokerHost = "test.mosquitto.org"
brokerHost = "localhost"
brokerPort = 1883
brokerIDontKnow = 60

#callback when we recieve a connack
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    
def on_message(client, userdata, msg):
    print(msg.topic + ": " + str(msg.payload))
    box = msg.topic.split("/")[1]
    name = msg.topic.split("/")[2]
    address = msg.topic.split("/")[3]

    if box == "inbox" and str(msg.payload) == "get" and address == "deviceInfo":
        client.publish("/outbox/"+clientName+"/deviceInfo", deviceJson)
    
def on_disconnect(client, userdata, rc):
    if rc != 0:
        print("Broker disconnection")
    time.sleep(10)
    client.connect(brokerHost, brokerPort, brokerIDontKnow)

client1 = mqtt.Client("in1054-segurancaUFPE1")
client1.on_connect = on_connect
client1.on_message = on_message
client1.on_disconnect = on_disconnect
client1.username_pw_set("","")
client1.connect(brokerHost, brokerPort, brokerIDontKnow)
client1.subscribe("/inbox/in1054-segurancaUFPE1/deviceInfo")
client1.publish("/outbox/in1054-segurancaUFPE1/deviceInfo", deviceJson) #for autoreconnect

client2 = mqtt.Client("in1054-segurancaUFPE2")
client2.on_connect = on_connect
client2.on_message = on_message
client2.on_disconnect = on_disconnect
client2.username_pw_set("","")
client2.connect(brokerHost, brokerPort, brokerIDontKnow)
client2.subscribe("/inbox/in1054-segurancaUFPE2/deviceInfo")
client2.publish("/outbox/in1054-segurancaUFPE2/deviceInfo", deviceJson) #for autoreconnect

client3 = mqtt.Client("in1054-segurancaUFPE3")
client3.on_connect = on_connect
client3.on_message = on_message
client3.on_disconnect = on_disconnect
client3.username_pw_set("","")
client3.connect(brokerHost, brokerPort, brokerIDontKnow)
client3.subscribe("/inbox/in1054-segurancaUFPE3/deviceInfo")
client3.publish("/outbox/in1054-segurancaUFPE3/deviceInfo", deviceJson) #for autoreconnect

######################################################
# FIM MQTT                                           #
######################################################

while step == 0 or traci.simulation.getMinExpectedNumber() > 0:
    #print "Simulation Time is", (traci.simulation.getCurrentTime())
    #print "stepping..."
    traci.simulationStep()
    #print "updating..."
    patrolCars.updateCars()
    
    if step == 200:
        patrolCars.removeCar("segurancaUFPE2")
    
    step = step+1
    
    
    ######################################################
    # MQTT                                               #
    ######################################################
    if not(step%5):
        for key, item in patrolCars.vehicles.iteritems():
            client = None
            
            if key == "segurancaUFPE1":
                client = client1
            elif key == "segurancaUFPE2":
                client = client2
            elif key == "segurancaUFPE3":
                client = client3
            
            if client:
                client.publish("/outbox/in1054-"+key+"/voltas", '{"value":'+str(item.routeCycleCompleted)+'}')
                speed = traci.vehicle.getSpeed(key);
                client.publish("/outbox/in1054-"+key+"/velocidade", '{"update": {"labels":["' + str(step) + '"],"series":[['+str(int(speed))+']]}}')
                client.publish("/outbox/in1054-"+key+"/rotas", '{"value":"'+ " | ".join(item.routes) +'"}')
            
    ######################################################
    # FIM MQTT                                           #
    ######################################################
    
    #print "ending..."