import os, subprocess, sys, random
sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..', '..', '..', 'tools'))
sys.path.append(os.path.join(os.environ.get("SUMO_HOME", os.path.join(os.path.dirname(__file__), '..', '..', '..')), 'tools'))
from sumolib import checkBinary
import sumolib
import traci
import random
import string

rede = sumolib.net.readNet("osm.net.xml")

class Singleton:
    """
    A non-thread-safe helper class to ease implementing singletons.
    This should be used as a decorator -- not a metaclass -- to the
    class that should be a singleton.

    The decorated class can define one `__init__` function that
    takes only the `self` argument. Other than that, there are
    no restrictions that apply to the decorated class.

    To get the singleton instance, use the `Instance` method. Trying
    to use `__call__` will result in a `TypeError` being raised.

    Limitations: The decorated class cannot be inherited from.

    """

    def __init__(self, decorated):
        self._decorated = decorated

    def Instance(self):
        """
        Returns the singleton instance. Upon its first call, it creates a
        new instance of the decorated class and calls its `__init__` method.
        On all subsequent calls, the already created instance is returned.

        """
        try:
            return self._instance
        except AttributeError:
            self._instance = self._decorated()
            return self._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `Instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)

@Singleton
class PatrolRouteCollection:
    def __init__(self):
        #Chave "nome da rota" / Valor "pontos da rota"
        self.routes = {}
        #Chave "nome da rota" / Valor "lista dos carros na rota"
        self.carInRoute = {}
        self.routeLength = {}

    def addPatrolRoute(self, name, points):
        self.routes[name] = points
        self.carInRoute[name] = []
        self.routeLength[name] = 0
    
    def assignCarToRoute(self, routeName, carName):
        self.carInRoute[routeName].append(carName)
    
    def removeCarFromRoute(self, routeName, carName):
        self.carInRoute[routeName].remove(carName)
        
    def getCarPerLengthRatio(self, name):
        return len(self.carInRoute[name]) / float(self.routeLength[name])
        
    def getLeastServedRoute(self):
        leastVal = sys.maxint
        leastName = None
        for key, item in self.routes.iteritems():
            routeVal = self.getCarPerLengthRatio(key)
            if routeVal < leastVal:
                leastVal = routeVal
                leastName = key
        return leastName

class PatrolVehicle:
    def __init__(self, name, routeName, step = None):
        if step == None:
            step = 1
        self.name = name
        
        if routeName != None:
            self.routes = [routeName]
            PatrolRouteCollection.Instance().assignCarToRoute(routeName, name)
        else:
            self.routes = []
        
        self.currentSource = -1
        self.needsRerouting = False
        
        self.originalPoints = None
        self.replacedBy = None
        self.routeLength = 0;
        self.routeCycleCompleted = 0;
        
        if len(self.routes) > 0:
            self.startCar(step)
    
    def startCar(self, step = None):
        if step == None:
            step = 1
        #print step
        self.currentSource = 0
        self.needsRerouting = True
        routeName = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8)) + ".start";
        traci.route.add(routeName, self.getPointsOfInterest()[0:2])
        traci.vehicle.add(self.name, routeName, depart=step, pos=0, speed=1, lane=0, typeID='DEFAULT_VEHTYPE')
        traci.vehicle.subscribe(self.name)
    
    def getPointsOfInterest(self):
        pointsOfInterest = []
        for item in self.routes:
            pointsOfInterest = pointsOfInterest + PatrolRouteCollection.Instance().routes[item]
            
        return pointsOfInterest

    def nextSource(self):
        pointsOfInterest = self.getPointsOfInterest()
        
        if self.currentSource+1 == len(pointsOfInterest):
            return 0
        else:
            return self.currentSource+1

    def update(self):
        if len(self.routes) > 0:
            if self.needsRerouting:
                stepData = traci.vehicle.getSubscriptionResults(self.name)
                currentRoute =  traci.vehicle.getRoute(self.name)
                
                #if self.name == "segurancaUFPE1":
                #    print stepData[80] + " " + str(currentRoute[-2])
                #if (stepData[80] != currentRoute[-2]):
                #    traci.vehicle.setRoute(self.name, [stepData[80], currentRoute[-1]])
                
                traci.vehicle.rerouteEffort(self.name)
                self.needsRerouting = False
            else:
                stepData = traci.vehicle.getSubscriptionResults(self.name)
                currNext = self.nextSource();
                pointsOfInterest = self.getPointsOfInterest()
                
                if (stepData[80] == pointsOfInterest[self.nextSource()]):
                    self.currentSource = currNext
                    newNext = self.nextSource()
                    next = [pointsOfInterest[currNext], pointsOfInterest[newNext]]
                    traci.vehicle.setRoute(self.name, next)
                    self.needsRerouting = True
                    
                    if currNext == 0:
                        if self.routeCycleCompleted == 0:
                            for item in traci.vehicle.getRoute(self.name):
                                self.routeLength = self.routeLength + rede.getEdge(item).getLength()
                                
                                if len(self.routes) == 1 and PatrolRouteCollection.Instance().routeLength[self.routes[0]] == 0:
                                        PatrolRouteCollection.Instance().routeLength[self.routes[0]] = self.routeLength
                                        #print PatrolRouteCollection.Instance().routeLength[self.routes[0]]
                                        #print PatrolRouteCollection.Instance().getCarPerLengthRatio(self.routes[0]);
                        
                        self.routeCycleCompleted = self.routeCycleCompleted + 1
                        
                    # if self.routeCycleCompleted == 0:
                        # currentFirstPointPos = rede.getEdge(pointsOfInterest[currNext]).getShape()[0]
                        # currentLastPointPos = rede.getEdge(pointsOfInterest[newNext]).getShape()[0]
                        # dist = sumolib.geomhelper.distance(currentFirstPointPos, currentLastPointPos)
                        
                        # self.routeLength = self.routeLength + dist
                        # print self.name + " " + str(self.routeLength)

@Singleton
class PatrolVehicleCollection:
    def __init__(self):
        self.vehicles = {}
    
    def addCar(self, car, color=None, isStart=False, step = None):
        if step == None:
            step = 1
        self.vehicles[car.name] = car
        
        if not(isStart):
            mostRoutesNum = sys.maxint
            mostRoutesCar = None
            for key, item in self.vehicles.iteritems():
                routesAssigned = len(item.routes)
                if routesAssigned > 1 and routesAssigned < mostRoutesNum:
                    mostRoutesNum = routesAssigned
                    mostRoutesCar = item
            
            if not(mostRoutesCar):
                targetRoute = PatrolRouteCollection.Instance().getLeastServedRoute()
                car.routes.append(targetRoute)

                PatrolRouteCollection.Instance().assignCarToRoute(targetRoute, car.name)
                car.startCar(step)
            else:
                stepData = traci.vehicle.getSubscriptionResults(mostRoutesCar.name)
                curr = stepData[80]
                print(curr)
                pointsOfInterest = mostRoutesCar.getPointsOfInterest()
                
                next = [curr, pointsOfInterest[0]]
                print(next)
                traci.vehicle.setRoute(mostRoutesCar.name, next)
                mostRoutesCar.needsRerouting = True
                mostRoutesCar.currentSource = -1
                
                newRoute = mostRoutesCar.routes[-1]
                del mostRoutesCar.routes[-1]  
                car.routes.append(newRoute)
                car.startCar(step)
        
        if color != None:
            traci.vehicle.setColor(car.name, color)
    
    def updateCars(self):
        for key, item in self.vehicles.iteritems():
            #print key
            item.update()
    
    def removeCar(self, carName):
        canRun = len([x for key, x in self.vehicles.iteritems() if x.routeCycleCompleted == 0]) == 0
        
        if canRun:
            car = self.vehicles[carName]
            
            removedCarPoints = car.getPointsOfInterest()
            removedFirstEdgePos = rede.getEdge(removedCarPoints[0]).getShape()[0]
            removedLastEdgePos = rede.getEdge(removedCarPoints[-1]).getShape()[0]
            
            distsFromTo = {}
            distsToFrom = {}
            
            smallerTotalDist = sys.maxint
            replacementPatrolCar = None
            
            for key, item in self.vehicles.iteritems():
                if item.name == carName:
                    continue
                    
                currentFirstPointPos = rede.getEdge(item.getPointsOfInterest()[0]).getShape()[0]
                currentLastPointPos = rede.getEdge(item.getPointsOfInterest()[-1]).getShape()[0]
                
                distsFromTo[item.name] = sumolib.geomhelper.distance(currentLastPointPos, removedFirstEdgePos)
                distsToFrom[item.name] = sumolib.geomhelper.distance(removedLastEdgePos, currentFirstPointPos)
            
            for key, item in self.vehicles.iteritems():
                if item.name == carName:
                    continue
                
                sum = distsFromTo[item.name] + distsToFrom[item.name] + item.routeLength
                
                if (sum < smallerTotalDist):
                    smallerTotalDist = sum
                    replacementPatrolCar = item.name
                
            #print replacementPatrolCar
            
            replacementPatrolCar = self.vehicles[replacementPatrolCar]
            #replacementPatrolCar.originalPoints = replacementPatrolCar.getPointsOfInterest()
            #replacementPatrolCar.pointsOfInterest = replacementPatrolCar.getPointsOfInterest() + removedCarPoints
            
            #print replacementPatrolCar.pointsOfInterest
            replacementPatrolCar.routeLength = 0
            replacementPatrolCar.routeCycleCompleted = 0
            car.replacedBy = replacementPatrolCar
            
            #traci.vehicle.setSpeed(car.name, 0)
            currentRoute = traci.vehicle.getRoute(car.name)  
            indexCurrentEdge = currentRoute.index(traci.lane.getEdgeID(traci.vehicle.getLaneID(car.name)))
            nextEdge = 0 if (indexCurrentEdge + 1 == len(currentRoute)) else indexCurrentEdge + 1
            
            #traci.vehicle.setStop(car.name, currentRoute[nextEdge])
            replacementPatrolCar.routes += car.routes
            for item in car.routes:
                PatrolRouteCollection.Instance().assignCarToRoute(item, replacementPatrolCar.name)
                PatrolRouteCollection.Instance().removeCarFromRoute(item, car.name)
            car.routes = []
            traci.vehicle.remove(car.name)
            #print("carro removido do traci")
            del PatrolVehicleCollection.Instance().vehicles[car.name]
            #print("carro removido da colecao")
        else:
            print("nao pode remover carros no momento")