print("starting...")

import os, subprocess, sys, random
sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..', '..', '..', 'tools'))
sys.path.append(os.path.join(os.environ.get("SUMO_HOME", os.path.join(os.path.dirname(__file__), '..', '..', '..')), 'tools'))
from sumolib import checkBinary
import sumolib
import traci
import patrolVehicle as in1054

PORT = 9999

NSGREEN = "GrGr"
NSYELLOW = "yryr"
WEGREEN = "rGrG"
WEYELLOW = "ryry"

PROGRAM = [WEYELLOW,WEYELLOW,WEYELLOW,NSGREEN,NSGREEN,NSGREEN,NSGREEN,NSGREEN,NSGREEN,NSGREEN,NSGREEN,NSYELLOW,NSYELLOW,WEGREEN]
sumoBinary = checkBinary('sumo-gui')
#sumoConfig = "data/Cologne.sumocfg"
#sumoConfig = "E:\\Doc\\veins-veins-4.4\\examples\\veins\\erlangen.sumo.cfg"
sumoConfig = "osm.sumocfg"

if len(sys.argv) > 1:
    huepath = '"%s" -c "%s" --python-script "%s" --remote-port %s' % (sumoBinary, sumoConfig, __file__, PORT)
    print(huepath)
    retCode = subprocess.call(huepath, shell=True, stdout=sys.stdout)
    print("Subproc called")
    sys.exit(retCode)
else:
    huepath = '"%s" -c "%s" --remote-port %s' % (sumoBinary, sumoConfig, PORT)
    print(huepath)
    sumoProcess = subprocess.Popen(huepath, shell=True, stdout=sys.stdout)
    print("\nSumo Popen")
    traci.init(PORT)
    print("\nTraCI init")

print("\nTraci Iniciado")
traci.gui.setZoom("View #0", 400)

traci.simulationStep()
programPointer = len(PROGRAM)-1
i=0
s=0
t=0
k=0
CTT5cars=[0,0,0,0,0]
step = 0
VEH=[]
EDG=[]
TIM=[]
VehCTT=[]
ED=[]
ITT=[]
V=[]
CTT=[]
Jun=[]
L_Veh=[]
Trav_tim=[]
VehiD=["0","1"]
Curr_Trav=[]
Trav_tim_sys=[]
A=[]
i = 0

All = traci.lane.getIDList()

NA=len(All)
print "NA iss", NA

for s in range(0,NA):
    LA=traci.lane.getLength(All[s])
    if LA>100:
        A.append(All[s])

##################################################

for i in range(0,len(A)):
    L=traci.lane.getLength(A[i]) # Get the Length of all Lane
    V=traci.lane.getMaxSpeed(A[i]) # Get Maximum Allowed speed
    ITT= L/V
    # Set ITT for Lanes
    ED= traci.lane.getEdgeID(A[i])
    traci.edge.setEffort(ED, ITT)
    CTT5cars=[ITT,ITT,ITT,ITT,ITT]
    CTTcheck=0

# Simulation starts here:
PontosInteresseVermelho = ["257975070#0", "249088920", "-248091833#0", "317078453#2"]
PontosInteresseAzul = ["257975070#5-AddedOnRampEdge", "248109372#0", "199429353", "199429358#1", "199428975#0"]
PontosInteresseVerde = ["199429358#6", "248096097#2", "245730591#1", "249100222", "249099950"]

in1054.PatrolRouteCollection.Instance().addPatrolRoute("vermelho", PontosInteresseVermelho)
in1054.PatrolRouteCollection.Instance().addPatrolRoute("azul", PontosInteresseAzul)
in1054.PatrolRouteCollection.Instance().addPatrolRoute("verde", PontosInteresseVerde)

patrolCars = in1054.PatrolVehicleCollection.Instance()

patrolCars.addCar(in1054.PatrolVehicle("segurancaUFPE1", "vermelho"), (255,0,0,0), True)
patrolCars.addCar(in1054.PatrolVehicle("segurancaUFPE2", "azul"), (0,255,255,255), True)
patrolCars.addCar(in1054.PatrolVehicle("segurancaUFPE3", "verde"), (0,255,0,0), True)

while step == 0 or traci.simulation.getMinExpectedNumber() > 0:
    #print "Simulation Time is", (traci.simulation.getCurrentTime())
    #print "stepping..."
    traci.simulationStep()
    #print "updating..."
    patrolCars.updateCars()
    
    if step == 200:
        patrolCars.removeCar("segurancaUFPE2")
        #print("carro removido completamente")
        # for key, item in in1054.PatrolRouteCollection.Instance().carInRoute.iteritems():
        #     print key + str(item)
        # print "1: " + str(patrolCars.vehicles["segurancaUFPE1"].routes)
        # print "2: " + str(patrolCars.vehicles["segurancaUFPE2"].routes)
        # print "3: " + str(patrolCars.vehicles["segurancaUFPE3"].routes)
        print "Passo 200"
    elif step == 250:
        patrolCars.addCar(in1054.PatrolVehicle("segurancaUFPE4", None), (255,255,255,255))
    #verificar pq com 400 quebra
    elif step == 450:
        patrolCars.addCar(in1054.PatrolVehicle("segurancaUFPE2", None), (0,255,255,255))
        
    
    step = step+1
    #print "ending..."