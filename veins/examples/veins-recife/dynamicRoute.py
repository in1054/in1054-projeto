print("starting...")

import os, subprocess, sys, random
sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..', '..', '..', 'tools'))
sys.path.append(os.path.join(os.environ.get("SUMO_HOME", os.path.join(os.path.dirname(__file__), '..', '..', '..')), 'tools'))
from sumolib import checkBinary
import traci

PORT = 9999

NSGREEN = "GrGr"
NSYELLOW = "yryr"
WEGREEN = "rGrG"
WEYELLOW = "ryry"

PROGRAM = [WEYELLOW,WEYELLOW,WEYELLOW,NSGREEN,NSGREEN,NSGREEN,NSGREEN,NSGREEN,NSGREEN,NSGREEN,NSGREEN,NSYELLOW,NSYELLOW,WEGREEN]
sumoBinary = checkBinary('sumo-gui')
#sumoConfig = "data/Cologne.sumocfg"
#sumoConfig = "E:\\Doc\\veins-veins-4.4\\examples\\veins\\erlangen.sumo.cfg"
sumoConfig = "E:\\CidadesConectadas\\2016-1\\IN1054\\Challenge1\\RecifeAntigo\\osm.sumocfg"

if len(sys.argv) > 1:
    huepath = '"%s" -c "%s" --python-script "%s" --remote-port %s' % (sumoBinary, sumoConfig, __file__, PORT)
    print(huepath)
    retCode = subprocess.call(huepath, shell=True, stdout=sys.stdout)
    print("Subproc called")
    sys.exit(retCode)
else:
    huepath = '"%s" -c "%s" --remote-port %s' % (sumoBinary, sumoConfig, PORT)
    print(huepath)
    sumoProcess = subprocess.Popen(huepath, shell=True, stdout=sys.stdout)
    print("\nSumo Popen")
    traci.init(PORT)
    print("\nTraCI init")

print("\nTraci Iniciado")
traci.simulationStep()
programPointer = len(PROGRAM)-1
i=0
s=0
t=0
k=0
CTT5cars=[0,0,0,0,0]
step = 0
VEH=[]
EDG=[]
TIM=[]
VehCTT=[]
ED=[]
ITT=[]
V=[]
CTT=[]
Jun=[]
L_Veh=[]
Trav_tim=[]
traci.edge.setEffort=[]
VehiD=["0","1"]
Curr_Trav=[]
Trav_tim_sys=[]
A=[]

sample_vehicle = ""
#print "sample vehicle Name is :", sample_vehicle

# Get the all Lane in Network
All = traci.lane.getIDList()
#print A

NA=len(All)
print "NA iss", NA

for s in range(0,NA):
    LA=traci.lane.getLength(All[s])
    if LA>100:
        A.append(All[s])
		
N=len(A)
print "N is", N
output = open ("output.txt", "w")
output.write ("\n\n\n")
output.write ("----------------------------------------------------------------------------------------\n")
output.write ("Simulation Time\t\t| Fuel Consumption\t\t | CO2 emissions \t\t | CO emssions\t\t | Noise emission \t\t | Current Speed \t\t | Current Lane\n")

# Calculate ITT for all Lanes
for i in range(0,N):
    L=traci.lane.getLength(A[i]) # Get the Length of all Lane
    print " Lane name is", A[i],"\t"
    print " is equal", L,"\n"
    V=traci.lane.getMaxSpeed(A[i]) # Get Maximum Allowed speed
    print "Maximum Allowed Speed for Lane", A[i],"\t"
    print " is", V,"\n"
    ITT= L/V
    print "ITT for Lane", A[i],"\t"
    print " is equal to", ITT,"\n"
    # Set ITT for Lanes
    ED= traci.lane.getEdgeID(A[i])
    traci.edge.setEffort = [A,ITT]
    CTT5cars=[ITT,ITT,ITT,ITT,ITT]

CTTcheck=0
	
# Simulation starts here:
while step == 0 or traci.simulation.getMinExpectedNumber() > 0:
    print "Simulation Time is", (traci.simulation.getCurrentTime())
    traci.simulationStep()
    programPointer = min(programPointer+1, len(PROGRAM)-1)
    
    # Calculate CTT for all Lanes
    
    for j in range(0,N):
        VEH=traci.lane.getLastStepVehicleIDs(A[j])
        EDG= traci.lane.getEdgeID(A[j])
        TIM= traci.simulation.getCurrentTime()
        VehiD[0]=VEH
		
        if VehiD[0]== '[0]':
            VehCTT=traci.vehicle.getAdaptedTraveltime(VehiD[0], TIM[j], EDG[j])
			
        VehCTT=traci.lane.getTraveltime(A[j])
		
        if CTTcheck == 0:
            CTT5cars[0]=VehCTT
            CTTcheck=1
        elif CTTcheck == 1:
            CTT5cars[1]=VehCTT
            CTTcheck=2
        elif CTTcheck == 2:
            CTT5cars[2]=VehCTT
            CTTcheck=3
        elif CTTcheck == 3:
            CTT5cars[3]=VehCTT
            CTTcheck=4
        else:
            CTT5cars[4]=VehCTT
            CTTcheck=0
		
	    CTT = sum(CTT5cars)/5
        print "Current Traveling Time (CTT) for Lane", A[j], "is equal to", CTT
        traci.edge.setEffort=[A[i],CTT]
        
        if sample_vehicle not in traci.vehicle.getIDList():
            print("\n\nCarros disponiveis: ")
            print (traci.vehicle.getIDList())
            sample_vehicle = raw_input("Enter sample vehicle Name: ")
			
			
        traci.vehicle.setColor(sample_vehicle, (255,0,0,0))
        #traci.vehicle.changeTarget(sample_vehicle, "29423002#9")
		
        CO=str(traci.vehicle.getCOEmission(sample_vehicle))
        Fue=str(traci.vehicle.getFuelConsumption(sample_vehicle))
        LAV=str(traci.vehicle.getLaneID(sample_vehicle))
        Noise=str(traci.vehicle.getNoiseEmission(sample_vehicle))
        curr_spe=str(traci.vehicle.getSpeed(sample_vehicle))
        speed_noTra=str(traci.vehicle.getSpeedWithoutTraCI(sample_vehicle))
        CO2=str(traci.vehicle.getCO2Emission(sample_vehicle))
        current_time = str(traci.simulation.getCurrentTime())
        output.write (current_time), output.write ("\t\t\t")
        output.write (Fue), output.write ("\t\t\t")
        output.write (CO2), output.write ("\t\t\t")
        output.write (CO), output.write ("\t\t\t")
        output.write (Noise), output.write ("\t\t\t")
        output.write (curr_spe), output.write ("\t\t\t")
        output.write (LAV), output.write ("\t\t\t")
        output.write (speed_noTra), output.write ("\t\t\t\n")
   
    # for Re-routing
   
    route = traci.vehicle.getRouteID(sample_vehicle)
    print "current route of sample vehicle is", route
    RoutEDG = traci.route.getEdges(route)
    print "RoutEDG", RoutEDG
    EDGN= len(RoutEDG)
    print EDGN
    veh_x,veh_y=traci.vehicle.getPosition(sample_vehicle)
    print "Vehicle position is", veh_x, veh_y
    Jun=traci.junction.getIDList()
    JunS= len(Jun)
    VEH_on_Inter = 0
    PN=0
	
    # check if Vehicle near the Intersection (less than 1 meter) for Re-routing
    if JunS <1:
        for k in range(0,JunS):
            jun_x,jun_y=traci.junction.getPosition(Jun[k])
            Distance_Veh_Edg = traci.simulation.getDistance2D( jun_x, jun_y, veh_x, veh_y, isGeo=False, isDriving=False)
			
        if Distance_Veh_Edg < 1:
            VEH_on_Inter = 1
			
        if VEH_on_Inter == 1:
            P_routes=traci.route.getSubscriptionResults()
            PN= len(P_routes)
            TIME= traci.simulation.getCurrentTime()
            VEH_on_Inter = 0
			
        for d in range(0,PN):
            P_routes_EDG=traci.lane.lanegetEdgeID(P_routes[d])
            Trav_tim = traci.vehicle.getEffort(sample_vehicle, TIME, P_routes_EDG[d])
            Curr_Trav = traci.vehicle.getAdaptedTraveltime(sample_vehicle, TIME,P_routes_EDG[d])
            Trav_tim_sys = traci.vehicle.rerouteTraveltime(sample_vehicle, TIME,P_routes_EDG[d])
        
        if Trav_tim < Curr_Trav:
            traci.vehicle.rerouteEffort(sample_vehicle)
        elif Trav_tim_sys < Curr_Trav:
            traci.vehicle.rerouteTraveltime(sample_vehicle)
   
    traci.vehicle.rerouteTraveltime(sample_vehicle)
	
no = traci.inductionloop.getLastStepVehicleNumber("0")
output.close()

if no > 0:
    programPointer = (0 if programPointer == len(PROGRAM)-1 else 3)
	
step += 1
traci.close()
sys.stdout.flush()