//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "veins/modules/application/traci/ControlCenter.h"

using Veins::TraCIScenarioManagerAccess;
using Veins::TraCIColor;

Define_Module(ControlCenter);
#define CAR_PREFIX  "testVehicle"


void ControlCenter::initialize(int stage) {
    if (stage == 0) {
        sentMessage = false;
        carAttend = "";
        carSubstitute = "";
        timeChangeRouteCenter = 0.0;
        timeChangeRouteVehicle = 0.0;

        occurrenceTime = 1 + (rand() % 200); //a ocorr�ncia pode ocorrer do tempo 1 at� o 200 segundos
        updateSentEvent = new cMessage("updateSent");
        scheduleAt(simTime() + occurrenceTime, updateSentEvent);
    }
}

void ControlCenter::handleMessage(cMessage *msg) {
    const simtime_t t = simTime();
    string info = msg->getName();
    cout << "ControlCenter, msg = " << info << endl;

    char sep = '$';
    vector<string> params = split2(info, sep);

    if((params.size() > 2) && (params[1].compare("received") == 0)){
        if(params[2].compare(carAttend) == 0){
            carAttend = "";

            if(timeChangeRouteCenter == 0.0){
                string timestr = t.str();
                double time = stod(timestr);
                double timeVehicle = stod(params[3]);

                timeChangeRouteCenter = time - occurrenceTime;
                timeChangeRouteVehicle = timeVehicle - occurrenceTime;
            }
        }
        if(params[2].compare(carSubstitute) == 0){
            carSubstitute = "";
        }
    }

    if(msg == updateAttendEvent){
        if(carAttend.compare("") == 0){
            delete msg;
        } else {
            sendMessageRsu(msg->getName());
            scheduleAt(simTime() + 0.05, updateAttendEvent);
        }

    } else if (msg == updateSubstituteEvent){
        if(carSubstitute.compare("") == 0){
            delete msg;
        } else {
            sendMessageRsu(msg->getName());
            scheduleAt(simTime() + 0.05, updateSubstituteEvent);
        }

    } else if (msg == updateSentEvent) {

        if(t == occurrenceTime){
            carAttend = "patrolVehicle0";
            string roadVehicle = "";

            int r = rand();

            if(r == 0){
                carSubstitute = "patrolVehicle1";
                roadVehicle = "blueRedRoute";
            } else {
                carSubstitute = "patrolVehicle2";
                roadVehicle = "blueGreenRoute";
            }

            string dataAttend = carAttend + sep + "changeRoute" + sep + roadVehicle;
            string dataSubstitute = carSubstitute + sep + "meetOccurrence";

            updateAttendEvent = new cMessage(dataAttend.c_str());
            scheduleAt(simTime() + 0.001, updateAttendEvent);

            updateSubstituteEvent = new cMessage(dataSubstitute.c_str());
            scheduleAt(simTime() + 0.002, updateSubstituteEvent);
        }
        if(t == (occurrenceTime + 400)){
            carAttend = "patrolVehicle0";
            string dataAttend = carAttend + sep + "originalRoute";

            updateAttendEvent = new cMessage(dataAttend.c_str());
            scheduleAt(simTime() + 0.001, updateAttendEvent);
        }

        scheduleAt(simTime() + 100.0, updateSentEvent);
    }
}

void ControlCenter::sendMessageRsu(string data) {
    //enviando mensagens paras as RSUs
    cMessage *msg2 = new cMessage(data.c_str());
    int n = gateSize("controlCenterOut"); //quantidade de RSUs
    for (int i = 0; i < n; i++){
        cMessage *copy = msg2->dup();
        send(copy, "controlCenterOut", i);
        //sendDelayed(msg, 0.005, "out");
    }
    delete msg2;
}

vector<string> ControlCenter::split2(string str, char delim) {
    vector<string> v;
    string tmp;
    string::const_iterator it;

    for(it = str.begin(); it <= str.end(); it++) {
        if(*it != delim && it != str.end()) {
            tmp += *it;
        } else {
            v.push_back(tmp);
            tmp = "";
        }
    }

    return v;
}

