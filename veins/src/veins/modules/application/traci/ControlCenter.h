//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SRC_VEINS_MODULES_APPLICATION_TRACI_CONTROLCENTER_H_
#define SRC_VEINS_MODULES_APPLICATION_TRACI_CONTROLCENTER_H_

#include <string.h>
#include <omnetpp.h>
#include <sstream>

#include "veins/base/utils/MiXiMDefs.h"
#include "veins/modules/mobility/traci/TraCIScenarioManager.h"
#include "veins/modules/mobility/traci/TraCICommandInterface.h"
#include "veins/modules/mobility/traci/TraCIColor.h"

using namespace omnetpp;
using namespace std;

using Veins::TraCIScenarioManager;
using Veins::TraCICommandInterface;

class ControlCenter : public cSimpleModule  {
protected:
    bool sentMessage;

    cMessage* updateSentEvent;
    cMessage* updateAttendEvent;
    cMessage* updateSubstituteEvent;

    string carAttend;
    string carSubstitute;

    TraCIScenarioManager* manager;
    TraCICommandInterface* traci;

    int occurrenceTime;
    double timeChangeRouteCenter;
    double timeChangeRouteVehicle;

public:
    list<string> blueRoute;
    list<string> redRoute;
    list<string> greenRoute;
    list<string> blueRedRoute;
    list<string> blueGreenRoute;

protected:
    virtual void initialize(int stage) override;
    virtual void handleMessage(cMessage *msg) override;
    virtual void sendMessageRsu(string data);
    virtual vector<string> split2(string str, char delim);

public:
    //ControlCenter();
    //virtual ~ControlCenter();
};

#endif /* SRC_VEINS_MODULES_APPLICATION_TRACI_CONTROLCENTER_H_ */
