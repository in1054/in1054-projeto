//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "veins/modules/application/traci/Link.h"

Define_Module(Link);

void Link::initialize(int stage) {
    if (stage == 0) {
        sentMessage = par("sentMessage");
    }
}

void Link::handleMessage(cMessage *msg) {
    string info = msg->getName();
    //cout << "Link, msg = " << info << endl;

    sentMessage = true;
    par("sentMessage").setBoolValue(sentMessage);
    par("message").setStringValue(info);
}
