//
// Copyright (C) 2006-2011 Christoph Sommer <christoph.sommer@uibk.ac.at>
//
// Documentation for these modules is at http://veins.car2x.org/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#include "veins/modules/application/traci/TraCIDemo11pv2.h"

using Veins::TraCIMobilityAccess;
using Veins::AnnotationManagerAccess;
using Veins::TraCIColor;

const simsignalwrap_t TraCIDemo11pv2::parkingStateChangedSignal = simsignalwrap_t(TRACI_SIGNAL_PARKING_CHANGE_NAME);

Define_Module(TraCIDemo11pv2);

void TraCIDemo11pv2::initialize(int stage) {
	BaseWaveApplLayer::initialize(stage);
	if (stage == 0) {
		mobility = TraCIMobilityAccess().get(getParentModule());
		traci = mobility->getCommandInterface();
		traciVehicle = mobility->getVehicleCommandInterface();
		annotations = AnnotationManagerAccess().getIfExists();
		ASSERT(annotations);

		sentMessage = false;
		lastDroveAt = simTime();
		findHost()->subscribe(parkingStateChangedSignal, this);
		isParking = false;
		sendWhileParking = par("sendWhileParking").boolValue();

		//updateIttEvent = new cMessage("updateITT");
		//scheduleAt(simTime() + 0.1, updateIttEvent);

		//updateCttEvent = new cMessage("updateCTT");
		//scheduleAt(simTime() + 1.0, updateCttEvent);

        updatePatrolCarEvent = new cMessage("updatePatrolCar");
        scheduleAt(simTime() + 1.0, updatePatrolCarEvent);

		returnOriginalRoute = false;

		initializePatrol();
	}
}

void TraCIDemo11pv2::onBeacon(WaveShortMessage* wsm) {

}

void TraCIDemo11pv2::onData(WaveShortMessage* wsm) {
	findHost()->getDisplayString().updateWith("r=16,blue");
	annotations->scheduleErase(1, annotations->drawLine(wsm->getSenderPos(), mobility->getPositionAt(simTime()), "blue"));

	//if (mobility->getRoadId()[0] != ':') traciVehicle->changeRoute(wsm->getWsmData(), 9999);
	//if (!sentMessage) sendMessage(wsm->getWsmData());

	treatMessage(wsm->getWsmData());
}

void TraCIDemo11pv2::sendMessage(std::string data) {
	sentMessage = true;
	t_channel channel = dataOnSch ? type_SCH : type_CCH;
	WaveShortMessage* wsm = prepareWSM("data", dataLengthBits, channel, dataPriority, -1,2);
	wsm->setWsmData(data.c_str());
	sendWSM(wsm);
}

void TraCIDemo11pv2::receiveSignal(cComponent* source, simsignal_t signalID, cObject* obj, cObject* details) {
	Enter_Method_Silent();
	if (signalID == mobilityStateChangedSignal) {
		handlePositionUpdate(obj);
	}
	else if (signalID == parkingStateChangedSignal) {
		handleParkingUpdate(obj);
	}
}

void TraCIDemo11pv2::handleParkingUpdate(cObject* obj) {
	isParking = mobility->getParkingState();
	if (sendWhileParking == false) {
		if (isParking == true) {
			(FindModule<BaseConnectionManager*>::findGlobalModule())->unregisterNic(this->getParentModule()->getSubmodule("nic"));
		}
		else {
			Coord pos = mobility->getCurrentPosition();
			(FindModule<BaseConnectionManager*>::findGlobalModule())->registerNic(this->getParentModule()->getSubmodule("nic"), (ChannelAccess*) this->getParentModule()->getSubmodule("nic")->getSubmodule("phy80211p"), &pos);
		}
	}
}

void TraCIDemo11pv2::handlePositionUpdate(cObject* obj) {
	BaseWaveApplLayer::handlePositionUpdate(obj);
	//const simtime_t t = simTime();

	// stopped for for at least 10s?
	if (mobility->getSpeed() < 1) {
		if (simTime() - lastDroveAt >= 10) {
			findHost()->getDisplayString().updateWith("r=16,red");
			if (!sentMessage) sendMessage(mobility->getRoadId());
		}
	}
	else {
		lastDroveAt = simTime();
	}

	//para a constru��o din�mica da rota
    string carName = traciVehicle->getVehicleId();
    //cout << "carName = " << carName << endl;
    if(activeVehicle == 1){
        if((carName.compare("patrolVehicle0") == 0) || (carName.compare("patrolVehicle1") == 0) || (carName.compare("patrolVehicle2") == 0)){
            string roadId = mobility->getRoadId();
            //cout << "roadId = " << roadId << endl;
            list<string> newRoute = this->checkForNewRoute(roadId);
            if(newRoute.size() > 0) {
                traciVehicle->changeVehicleRoute(newRoute);
                traciVehicle->rerouteEffort();
            }
        }
    }

//    if((t == 150) && (carName.compare("patrolVehicle2") == 0)){
//        emergency = true;
//        activeVehicle = 0;
//    }
//    if((t == 150) && (carName.compare("patrolVehicle0") == 0)){
//        route = blueGreenRoute;
//        traciVehicle->changeVehicleRoute(blueGreenRoute);
//    }
//    if((t == 150) && (carName.compare("patrolVehicle1") == 0)){
//        emergency = true;
//        activeVehicle = 0;
//    }
//    if((t == 150) && (carName.compare("patrolVehicle0") == 0)){
//        route = blueRedRoute;
//        traciVehicle->changeVehicleRoute(blueRedRoute);
//    }
//    if((t == 350) && (carName.compare("patrolVehicle0") == 0)){
//        returnOriginalRoute = true;
//        if(route == blueRedRoute){
//            traci->addVehicle("patrolVehicle1", "vtype0", "redRouteStart");
//            traci->vehicle("patrolVehicle1").setColor(TraCIColor::fromTkColor("red"));
//        }
//        if(route == blueGreenRoute){
//            traci->addVehicle("patrolVehicle2", "vtype0", "greenRouteStart");
//            traci->vehicle("patrolVehicle2").setColor(TraCIColor::fromTkColor("green"));
//        }
//    }
    if(returnOriginalRoute && (carName.compare("patrolVehicle0") == 0)){
        if(checkOriginalRoute()){
            returnOriginalRoute = false;
            route = blueRoute;
            traciVehicle->changeVehicleRoute(blueRoute);
        }
    }
}

void TraCIDemo11pv2::sendWSM(WaveShortMessage* wsm) {
	if (isParking && !sendWhileParking) return;
	sendDelayedDown(wsm,individualOffset);
}

void TraCIDemo11pv2::handleSelfMsg(cMessage* msg) {

    if(msg == updatePatrolCarEvent){

        string carName = traciVehicle->getVehicleId();
        if((carName.compare("patrolVehicle0") == 0) || (carName.compare("patrolVehicle1") == 0) || (carName.compare("patrolVehicle2") == 0)){
            //string roadId = traciVehicle->getRoadId();

            string roadId = mobility->getRoadId();
            double speed = mobility->getSpeed();
            Coord position = mobility->getCurrentPosition();

            string sep = "$";
            string str_speed = to_string(speed);
            string str_px = to_string(position.x);
            string str_py = to_string(position.y);
            string str_pz = to_string(position.z);
            string data = "center" + sep + carName + sep + str_speed + sep + str_px + sep + str_py + sep + str_pz + sep + roadId;

            sendMessage(data);

            scheduleAt(simTime() + 5.0, updatePatrolCarEvent);
        } else {
            delete msg;
        }

    }else if(msg == updateIttEvent){
        if(simTime() <= 0.5){
            computeITT();
        }
    }else if(msg == updateCttEvent){
        computeCTT();
        scheduleAt(simTime() + 1.0, updateCttEvent);
    }else{
        BaseWaveApplLayer::handleSelfMsg(msg);
    }
}

void TraCIDemo11pv2::treatMessage(string msg) {
    char sep = '$';
    vector<string> params = split2(msg, sep);

    string carName = traciVehicle->getVehicleId();

    if(carName.compare(params[0]) == 0){ //se a mensagem � destinada a esse ve�culo

        if(params[1].compare("changeRoute") == 0){ //mudan�a de rota
            //configura a nova rota
            if(params[2].compare("blueRoute") == 0){
                route = blueRoute;
            }else if(params[2].compare("redRoute") == 0){
                route = redRoute;
            }else if(params[2].compare("greenRoute") == 0){
                route = greenRoute;
            }else if(params[2].compare("blueRedRoute") == 0){
                route = blueRedRoute;
            }else if(params[2].compare("blueGreenRoute") == 0){
                route = blueGreenRoute;
            }
            traciVehicle->changeVehicleRoute(route);

        }else if(params[1].compare("originalRoute") == 0){ //retornar a rota original
            returnOriginalRoute = true;
            if(route == blueRedRoute){
                traci->addVehicle("patrolVehicle1", "vtype0", "redRouteStart");
                traci->vehicle("patrolVehicle1").setColor(TraCIColor::fromTkColor("red"));
            }
            if(route == blueGreenRoute){
                traci->addVehicle("patrolVehicle2", "vtype0", "greenRouteStart");
                traci->vehicle("patrolVehicle2").setColor(TraCIColor::fromTkColor("green"));
            }

        }else if(params[1].compare("meetOccurrence") == 0){ //atender a uma ocorr�ncia
            activeVehicle = 0;
        }

        string timestr = simTime().str();
        string sep = "$";
        string data =  "center" + sep + "received" + sep + carName + sep + timestr;
        sendMessage(data);
    }
}

void TraCIDemo11pv2::computeCTT() {
    string roadId = traciVehicle->getRoadId();
    Veins::TraCICommandInterface::Road traciRoad = traci->road(roadId);

    string timestr = simTime().str();
    double time = stod(timestr);

    double vehicleCTT = traciRoad.getTravelTime(time);
    traciRoad.setEffort(vehicleCTT);

//    string road1 = "3/3to2/3";
//    traciRoad = traci->road(road1);
//    traciRoad.setEffort(5.0);
}

void TraCIDemo11pv2::computeITT(){
    list<string> laneIds = traci->getLaneIds();
    int cont = 0;

    for(auto it = laneIds.begin(); it != laneIds.end(); it++){
        string laneId = *it;

        Veins::TraCICommandInterface::Lane traciLane = traci->lane(laneId);
        double L = traciLane.getLength();
        double V = traciLane.getMaxSpeed();
        double ITT = L / V;

        if(L > 100.0){ //apenas paras as lanes com comprimento maior que 100
            string roadId = traciLane.getRoadId();
            Veins::TraCICommandInterface::Road traciRoad = traci->road(roadId);

            traciRoad.setEffort(ITT);

            //string timestr = simTime().str();
            //double time = stod(timestr);
            //double valor = traciRoad.getEffort(time);

            cont++;
        }
    }

    cout << "numero de lanes com comprimento maior que 100 = " << cont << endl;
}

vector<string> TraCIDemo11pv2::split2(string str, char delim) {
    vector<string> v;
    string tmp;
    string::const_iterator it;

    for(it = str.begin(); it <= str.end(); it++) {
        if(*it != delim && it != str.end()) {
            tmp += *it;
        } else {
            v.push_back(tmp);
            tmp = "";
        }
    }

    return v;
}

void TraCIDemo11pv2::initializePatrol() {

        this->blueRoute.push_back("257975070#5-AddedOnRampEdge");
        this->blueRoute.push_back("248109372#0");
        this->blueRoute.push_back("199429353");
        this->blueRoute.push_back("199429358#1");
        this->blueRoute.push_back("199428975#0");

        this->redRoute.push_back("257975070#0");
        this->redRoute.push_back("249088920");
        this->redRoute.push_back("-248091833#0");
        this->redRoute.push_back("317078453#2");
        this->redRoute.push_back("317078453#4");

        this->greenRoute.push_back("199429358#6");
        this->greenRoute.push_back("248096097#2");
        this->greenRoute.push_back("245730591#1");
        this->greenRoute.push_back("249100222");
        this->greenRoute.push_back("249099950");
        this->greenRoute.push_back("257975070#0");
        this->greenRoute.push_back("317078448#1");

        this->blueRedRoute.push_back("257975070#5-AddedOnRampEdge");
        this->blueRedRoute.push_back("248109372#0");
        this->blueRedRoute.push_back("199429353");
        this->blueRedRoute.push_back("199429358#1");
        this->blueRedRoute.push_back("199428975#0");
        this->blueRedRoute.push_back("317078453#1");
        this->blueRedRoute.push_back("317078453#4");
        this->blueRedRoute.push_back("257975070#0");
        this->blueRedRoute.push_back("249088920");
        this->blueRedRoute.push_back("-248091833#0");

        this->blueGreenRoute.push_back("257975070#5-AddedOnRampEdge");
        this->blueGreenRoute.push_back("248109372#0");
        this->blueGreenRoute.push_back("199429353");
        this->blueGreenRoute.push_back("199429358#1");
        this->blueGreenRoute.push_back("199429358#6");
        this->blueGreenRoute.push_back("248096097#2");
        this->blueGreenRoute.push_back("245730591#1");
        this->blueGreenRoute.push_back("249100222");
        this->blueGreenRoute.push_back("249099950");
        this->blueGreenRoute.push_back("257975070#0");

        string carName = traciVehicle->getVehicleId();
        if(carName.compare("patrolVehicle0") == 0){
            route = blueRoute;
        } else if(carName.compare("patrolVehicle1") == 0){
            route = redRoute;
        } else if(carName.compare("patrolVehicle2") == 0){
            route = greenRoute;
        }

        activeVehicle = 1; //para saber se um ve�culo de patrulha saiu para verificar uma ocorr�ncia
}

list<string> TraCIDemo11pv2::checkForNewRoute(std::string roadId) {
    list<string> newRoute;
    char setNext = 0;

    for (list<string>::iterator it2 = route.begin(); it2 != route.end(); it2++) {
        string temp = *it2;

        if(setNext == 1) {
            newRoute.push_back(roadId);
            newRoute.push_back(temp);
            return newRoute;
        }

        if(temp.compare(roadId) == 0){
            if(roadId.compare(route.back()) == 0) {
                newRoute.push_back(roadId);
                newRoute.push_back(route.front());
                return newRoute;
            } else {
                setNext = 1;
            }
        }
    }
    return newRoute;
}

bool TraCIDemo11pv2::checkOriginalRoute() {
    string carName = traciVehicle->getVehicleId();
    string roadId = mobility->getRoadId();
    list<string> testeRoute;

    if(carName.compare("patrolVehicle0") == 0){
        testeRoute = blueRoute;
    } else if(carName.compare("patrolVehicle1") == 0){
        testeRoute = redRoute;
    } else if(carName.compare("patrolVehicle2") == 0){
        testeRoute = greenRoute;
    }

    for (list<string>::iterator it2 = testeRoute.begin(); it2 != testeRoute.end(); it2++) {
        string temp = *it2;
        if(temp.compare(roadId) == 0){
            return true;
        }
    }
    return false;
}

