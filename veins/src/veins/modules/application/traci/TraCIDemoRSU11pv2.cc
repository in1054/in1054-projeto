//
// Copyright (C) 2006-2011 Christoph Sommer <christoph.sommer@uibk.ac.at>
//
// Documentation for these modules is at http://veins.car2x.org/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#include "veins/modules/application/traci/TraCIDemoRSU11pv2.h"

using Veins::AnnotationManagerAccess;

using Veins::TraCIScenarioManagerAccess;

Define_Module(TraCIDemoRSU11pv2);

void TraCIDemoRSU11pv2::initialize(int stage) {
	BaseWaveApplLayer::initialize(stage);
	if (stage == 0) {
		mobi = dynamic_cast<BaseMobility*> (getParentModule()->getSubmodule("mobility"));
		ASSERT(mobi);
		annotations = AnnotationManagerAccess().getIfExists();
		ASSERT(annotations);
		sentMessage = false;

		updatePatrolRsuEvent = new cMessage("updatePatrolRsu");
		scheduleAt(simTime() + 1.0, updatePatrolRsuEvent);
	}
}

void TraCIDemoRSU11pv2::onBeacon(WaveShortMessage* wsm) {

}

void TraCIDemoRSU11pv2::onData(WaveShortMessage* wsm) {
	findHost()->getDisplayString().updateWith("r=16,green");
	annotations->scheduleErase(1, annotations->drawLine(wsm->getSenderPos(), mobi->getCurrentPosition(), "blue"));

	//if (!sentMessage) sendMessage(wsm->getWsmData());

	char sep = '$';
	vector<string> params = split2(wsm->getWsmData(), sep);

	if(params[0].compare("center") == 0){ //verifica se a mensagem deve ser enviada ao centro de controle
	    sendMessageControlCenter(wsm->getWsmData());
	}
}

void TraCIDemoRSU11pv2::sendMessage(std::string data) {
	sentMessage = true;
	t_channel channel = dataOnSch ? type_SCH : type_CCH;
	WaveShortMessage* wsm = prepareWSM("data", dataLengthBits, channel, dataPriority, -1,2);
	wsm->setWsmData(data.c_str());
	sendWSM(wsm);
}

void TraCIDemoRSU11pv2::sendWSM(WaveShortMessage* wsm) {
	sendDelayedDown(wsm, individualOffset);
}

void TraCIDemoRSU11pv2::sendMessageControlCenter(std::string data) {
    cMessage *msg = new cMessage(data.c_str());
    cModule *targetModule = getParentModule()->getSubmodule("connection");

    cGate *outGate = NULL;
    if(targetModule->hasGate("linkOut")){
        outGate = targetModule->gate("linkOut");
    }

    if(outGate != NULL){
        send(msg, outGate);
        //sendDelayed(msg, 0.005, "out");
    }else{
        cout << "TraCIDemoRSU11pv2::sendMessageControlCenter, gate no found." << endl;
    }
}

void TraCIDemoRSU11pv2::handleSelfMsg(cMessage* msg) {

    if(msg == updatePatrolRsuEvent){

        //checa se tem mensagem para enviar aos carros
        cModule *targetModule = getParentModule()->getSubmodule("connection");
        if(targetModule->par("sentMessage").boolValue()){
            string message = targetModule->par("message").stdstringValue();
            targetModule->par("sentMessage").setBoolValue(false);
            sendMessage(message);
        }

        scheduleAt(simTime() + 0.05, updatePatrolRsuEvent);

    }else{
        BaseWaveApplLayer::handleSelfMsg(msg);
    }
}

vector<string> TraCIDemoRSU11pv2::split2(string str, char delim) {
    vector<string> v;
    string tmp;
    string::const_iterator it;

    for(it = str.begin(); it <= str.end(); it++) {
        if(*it != delim && it != str.end()) {
            tmp += *it;
        } else {
            v.push_back(tmp);
            tmp = "";
        }
    }

    return v;
}
